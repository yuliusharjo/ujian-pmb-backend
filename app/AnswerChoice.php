<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class AnswerChoice extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'index',
        'choice_text',
        'choice_image_id',
        'choice_image_name',
        'correct_choice',
        'question_id'
    ];

    protected $dates = ['deleted_at'];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id')->withTrashed();
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'choice_image_id')->withTrashed();
    }
}
