<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnswerSummary extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'exam_detail_id',
        'category_id',
        'questions_total',
        'correct_answers'
    ];

    protected $dates = ['deleted_at'];

    public function examDetail()
    {
        return $this->belongsTo(ExamDetail::class, 'exam_detail_id')->withTrashed();
    }

    public function category()
    {
        return $this->belongsTo(QuestionCategory::class, 'category_id')->withTrashed();
    }
}
