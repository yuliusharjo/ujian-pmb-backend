<?php

namespace App\Docz;

use App\Question;
use App\QuestionDiscourse;
use Illuminate\Support\Collection;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\ListItem;

class CategoryGenerator
{
    /** @var Section */
    private $cat = NULL;
    public $counter = 0;
    private $questions = NULL;   
    public $name;

    public function __construct(Section $cat = NULL, $counter = 0) {
        $this->cat = $cat;
        $this->questions = new Collection();
        $this->counter = $counter;
        return $this;
    }

    public function AddHint(string $petunjuk)
    {
        $tinside = '<table style="width: 100%; border: 40pt #FFFFFF dotted;">'.
                        "<tr><td>$petunjuk</td></tr>".
                    '</table>';
        $toutside = '<table style="border: 20pt #000000 solid;"><tr>'."<td>$tinside</td>".'</tr></table>';
        
        Html::addHtml($this->cat, $toutside, false, false);
        $this->cat->addTextBreak();
        return $this;
    }
    
    public function AddDiscourse(QuestionDiscourse $discourse)
    {
        Html::addHtml($this->cat, "<p><b>$discourse->title:</b></p> $discourse->body ", false, false);
        $this->cat->addTextBreak();
        return $this;
    }

    public function NewQuestion(Question $question)
    {
        $index = $this->counter + $this->questions->count() + 1;
        $gen = new QuestionGenerator($this->cat);
        $gen->AddQuestion($question, $index);
        $this->questions->add($question->question);
        return $gen;
    }
}
