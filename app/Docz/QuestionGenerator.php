<?php

namespace App\Docz;

use App\Question;
use Illuminate\Support\Collection;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\SimpleType\Jc;

class QuestionGenerator
{
    /** @var Section */
    private $cat = NULL;
    public $id = 0;

    public function __construct(Section $cat)
    {
        $this->cat = $cat;
        return $this;
    }

    public function AddQuestion(Question $question, $index)
    {
        Html::addHtml($this->cat, '<runp>' . $question->question . '</runp>', false, true);
        //&& file_exists(public_path("/images/$choice->choice_image_name"))
        if (!empty($question->question_image_name)) {
            try {
                $this->cat->addTextBreak(1, NULL, ['lineHeight' => 0.5]);
                $this->cat->addImage("https://ujian-pmb.uajy.ac.id/pmbapi/images/$question->question_image_name", [
                    'wrappingStyle' => 'inline',
                    'alignment' => Jc::CENTER,
                ], 200);
            } catch (\Throwable $th) {
                //throw $th;
            }
            $this->cat->addTextBreak(1, NULL, ['lineHeight' => 0.5]);
        }
        return $this;
    }

    public function AddAnswerChoice(Collection $choices, Section $correctChoices)
    {
        //! Bakal error jika question merupakan html!
        
        $letter = 'A';
        $no_answer = true;
        foreach ($choices as $choice) {         
            if(empty($choice->choice_text) && empty($choice->choice_image_name)){ continue;}
            if ($choice->correct_choice) {
                $correctChoices->addListItem($letter, 0, NULL, 'multilevel', [
                    'align' => 'LEFT',
                    'indentation' => array('left' => 240, 'hanging' => 240)
                ]);
                $no_answer = false;
            }
            Html::addHtml($this->cat, '<runpx>' . $choice->choice_text . '</runp>', false,  false);
            
            //&& file_exists(public_path("/images/$choice->choice_image_name"))
            if (!empty($choice->choice_image_name)) {
                try {
                    if (empty($choice->choice_text)) {
                        $this->cat->addImage("https://ujian-pmb.uajy.ac.id/pmbapi/images/$choice->choice_image_name", [
                            'marginTop'        => -25,
                            'wrappingStyle' => \PhpOffice\PhpWord\Style\Image::WRAP_INLINE,
                            'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE,
                            'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
                            'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
                            'posVertical'      => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_CENTER,
                            'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_TEXT,
                        ], 48);
                    } else {
                        $this->cat->addImage("https://ujian-pmb.uajy.ac.id/pmbapi/images/$choice->choice_image_name", [
                            'marginTop'        => 0,
                            'wrappingStyle' => \PhpOffice\PhpWord\Style\Image::WRAP_TOPBOTTOM,
                            'alignment' => Jc::CENTER
                        ], 48);
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
            $letter++;
        }

        if ($no_answer) {
            $correctChoices->addListItem('No Right Answer!', 0, NULL, 'multilevel', [
                'align' => 'LEFT',
                'indentation' => array('left' => 240, 'hanging' => 240)
            ]);
            $no_answer = false;
        }
        $this->cat->addTextBreak();
        return $this;
    }
}
