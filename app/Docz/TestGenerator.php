<?php

namespace App\Docz;

use App\AnswerChoice;
use App\Docz\WordGenerator;
use App\Http\Controllers\Controller;
use App\Image;
use App\Jobs\GenerateDocz;
use App\Question;
use App\QuestionCategory;
use App\QuestionDiscourse;
use App\QuestionPacket;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class TestGenerator extends Controller
{
    private $args = ['name' => 'test-3.0', 'packet' => 1];
    public function __construct()
    {
        $start = microtime(true);
        $document = new WordGenerator();
        $document->GeneratePacket($this->args['packet']);
        $docpath = 'storage/app/docz/'.$this->args['name'].'.docx';
        $keypath = 'storage/app/docz/'.$this->args['name'].'-key.docx';
        $document->Save($docpath, $keypath);
                        
        dump('Complete Jobs , '. $this->args['name']);
        $time_elapsed_secs = microtime(true) - $start;
        dump($time_elapsed_secs);
    }
}
