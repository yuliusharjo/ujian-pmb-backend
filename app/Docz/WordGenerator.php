<?php

namespace App\Docz;

use App\AnswerChoice;
use App\Image;
use App\Question;
use App\QuestionCategory;
use App\QuestionDiscourse;
use App\QuestionPacket;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\JcTable;
use PhpOffice\PhpWord\SimpleType\TextAlignment;
use PhpOffice\PhpWord\SimpleType\VerticalJc;
use PhpOffice\PhpWord\Style\Font;
use ZipArchive;

class WordGenerator
{
    /** @var PhpWord */
    private $document = NULL;
    /** @var Collection */
    private $categories = NULL;
    private $code = '';
    /** @var PhpWord */
    private $keys = NULL;

    public function __construct()
    {
        $this->document = new PhpWord();
        $this->categories = new Collection();
        $this->keys = new PhpWord();
    }

    public function GeneratePacket(int $packet_id)
    {
        $packet = QuestionPacket::find($packet_id);
        $this->Start($packet->packet_code);
        $_questions = $packet->questions;
        $categories = $_questions->pluck('category_id')->unique()->values()->all();

        $counter = 0;
        foreach ($categories as $index) {

            /**
             * TODO:
             * 1. dapatkan semua petunjuk yang sama
             * 2. pertanyaan dengan petunjuk yang sama di kelompokan
             * 3. pertanyaan dibedakan menjadi yg berdiscourse dan tidak.
             * 4. ezzz..  ヾ(^▽^*)))
             */

            $category = QuestionCategory::find($index);
            $countQuestions = $_questions->where('category_id', $index)->count();
            //! ambil semua hint (grouped)
            $hints = $_questions->where('category_id', $index)->groupBy('hint');
            //! yg gk ada petunjuk pisahin, trus loop
            $nohints = $hints->pull("");
            if ($nohints == NULL) $nohints = collect([]);
            //! pake kalo ada kemungkinan petunjuk itu kosong.
            //! ambil wacana dari chunk agar random yg lebih baik
            $chunkdiscourse = $nohints->groupBy('discourse_id');
            $chunknodiscourse = $chunkdiscourse->pull("");
            if ($chunknodiscourse == NULL) $chunknodiscourse = collect([]);

            $hints = $hints->shuffle();
            $hints->add($chunkdiscourse->flatten(1));
            $hints->add($chunknodiscourse->chunk(5)->flatten(1));
            //! klo kurang dari 5
            $this->keys->getSection(0)->addText($category->category, ['bold' => true, 'allCaps' => true]);
            $generator = $this->NewCategory($category->category, $counter);
            //! ambil pertanyaan dengan hint yang sama
            //! loop hint 
            $this->LoopHint($hints, $generator);

            $this->keys->getSection(0)->addTextBreak();
            $counter += $countQuestions;
        }
    }

    private function LoopHint(Collection $hints, CategoryGenerator $generator)
    {
        $first_hint = true;
        $hints->each(function ($hint, $key) use ($generator, $first_hint) {
            //! count untuk loop
            //! agar lebih cepat close aja kalo 0
            $countInsideHints = $hint->count();
            if ($countInsideHints > 0) {
                //! ambil wacana dan tidak
                $discourses = $hint->groupBy('discourse_id');
                $nodiscourse = $discourses->pull("");
                if ($discourses == NULL) $discourses = collect([]);
                if ($nodiscourse == NULL) $nodiscourse = collect([]);
                $discourses->shuffle();
                $nodiscourse->shuffle();


                //! pertanyaan random dari keduanya🎲
                for ($i = 0; $i < $countInsideHints; $i++) {
                    //* 1,5,3,6 itu tanpa wacana
                    $dice = rand(1, 6);
                    switch ($dice) {
                        case 1:
                        case 5:
                        case 3:
                        case 6:
                            if ($nodiscourse->count() === 0) $this->CreateQuestion($discourses, $generator, $first_hint);
                            else $this->CreateQuestion($nodiscourse, $generator, $first_hint);
                            break;
                        case 2:
                        case 4:
                            if ($discourses->count() === 0) $this->CreateQuestion($nodiscourse, $generator, $first_hint);
                            else $this->CreateQuestion($discourses, $generator, $first_hint);
                            break;
                    }
                    $first_hint = false;
                }
            }
        });
    }

    protected function CreateQuestion(Collection $collection, CategoryGenerator $generator, $first_hint = true)
    {
        $first_discourse = true;
        if ($collection->count() === 0) return;
        $popitem = $collection->pop();

        if ($popitem instanceof Question) $popitem = [$popitem];
        else if ($popitem instanceof Collection) $popitem = $popitem->all();

        foreach ($popitem as $question) {
            if (!empty($question->hint) && $first_hint) $generator->AddHint($question->hint);
            if (!empty($question->discourse_id)) $discourse = QuestionDiscourse::find($question->discourse_id);
            if (!empty($discourse) && $first_discourse) $generator->AddDiscourse($discourse);

            $image = Image::find($question->question_image_id);
            if (!empty($image)) $question->{'question_image_name'} = $image->file_name;
            else $question->{'question_image_name'} = NULL;

            $questionchoice = AnswerChoice::where('question_id', $question->id)->with('image')->inRandomOrder()->get();
            $generator->NewQuestion($question)->AddAnswerChoice($questionchoice, $this->keys->getSection(0));
            $first_discourse = false;
        }
    }

    protected function Start(string $code)
    {
        $this->document->addSection()->addPageBreak();

        $this->code = $code;
        $this->document->setDefaultFontName('Times New Roman');
        $this->document->setDefaultFontSize(11);
        $this->document->setDefaultParagraphStyle(array('keepNext' => true, 'alignment' => Jc::START));

        $this->keys->addSection(array('breakType' => 'continuous', 'colsNum' => 3))
            ->addText('Kunci Jawaban', ['size' => 14, 'bold' => true]);
        $multilevelNumberingStyleName = 'multilevel';

        $this->document->addParagraphStyle('nospace', array('align' => 'both', 'spaceBefore' => 0, 'spaceAfter' => 0, 'spacing' => 0, 'lineHeight' => 0.001));

        $this->keys->addNumberingStyle(
            $multilevelNumberingStyleName,
            array(
                'type'   => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                )                
            )
        );
        return $this;
    }

    protected function NewCategory(string $name, $counter = 0)
    {
        //* membuat section baru.
        //* halaman kosong
        $category = $this->document->addSection();
        $header = $category->addHeader();
        $header->firstPage();
        $footer = $category->addFooter();
        $footer->firstPage();
        $category->addText("Halaman kosong untuk $name", NULL, ['pageBreakBefore' => true]);
        $category->addPageBreak();
        //* halaman pertanyaan
        $category->setStyle(array('breakType' => 'continuous', 'colsNum' => 2));
        //* tambah header
        $index = $this->categories->count();
        $subsequent = $category->addHeader();
        $table = $subsequent->addTable(['cellMargin' => 5, 'borderSize' => 20, 'borderColor' => '000000', 'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT, 'width' => 100 * 50]);
        $table->addRow();
        $cell = $table->addCell(60, ['borderSize' => 0, 'vAlign' => VerticalJc::CENTER]);
        $textrun = $cell->addTextRun([
            'alignment' => JcTable::START,
            'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(0.05),
            'spaceBefore' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(0.05)
        ]);
        $textrun->addText("\t$name", ['bold' => true, 'size' => 11, 'allCaps' => true]);
        $cell = $table->addCell(40, ['borderSize' => 0, 'vAlign' => VerticalJc::CENTER]);
        $textrun = $cell->addTextRun([
            'alignment' => JcTable::END,
            'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(0.05),
            'spaceBefore' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(0.05)
        ]);
        $textrun->addText("$this->code\t", ['bold' => true, 'size' => 11, 'allCaps' => true]);
        //* add footer
        $footer = $category->addFooter();
        $footer->addPreserveText('Halaman {PAGE} dari {NUMPAGES}.', null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::END));
        //* tambah ke counter
        $this->categories->add(['index' => $index, 'name' => $name]);
        return new CategoryGenerator($category, $counter);
    }

    protected function FromCategory(string $name)
    {
        $findSection = NULL;
        $this->categories->map(function ($item) use ($name, $findSection) {
            if ($item->name === $name) $findSection = $this->document->getSection($item->index);
        });
        if (empty($section)) return $this->NewCategory($name + $this->categories->count());
        return new CategoryGenerator($findSection);
    }

    public function Save(string $docpath, string $keypath)
    {
        try {
            $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->document, 'Word2007');
            $xmlWriter->save("$docpath");

            $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->keys, 'Word2007');
            $xmlWriter->save("$keypath");

            $files = array($docpath, $keypath);
            $filenames = array('keys.docx', 'document.docx');
            $zipname = "$docpath.zip";
            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);
            foreach ($files as $file) {
                $zip->addFile($file, array_pop($filenames));
            }
            $zip->close();
        } catch (\Throwable $th) {
            // do nothing
        }
    }
}
