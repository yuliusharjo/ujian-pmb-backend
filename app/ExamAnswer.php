<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamAnswer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'question_id',
        'choice_index',
        'is_correct',
        'exam_detail_id',
        'exam_type'
    ];

    protected $dates = ['deleted_at'];

    public function examDetail()
    {
        return $this->belongsTo(ExamDetail::class, 'exam_detail_id')->withTrashed();
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id')->withTrashed();
    }
}
