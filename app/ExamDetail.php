<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'participant_id',
        'packet_id',
        'schedule_id',
        'level',
        'idv_cut_off_time',
        'total_score',
        'accepted_in'
    ];

    protected $dates = ['deleted_at'];

    public function participant()
    {
        return $this->belongsTo(Participant::class, 'participant_id')->withTrashed();
    }

    public function questionPacket()
    {
        return $this->belongsTo(QuestionPacket::class, 'packet_id')->withTrashed();
    }

    public function schedule()
    {
        return $this->belongsTo(ExamSchedule::class, 'schedule_id')->withTrashed();
    }

    public function examAnswers()
    {
        return $this->hasMany(ExamAnswer::class, 'exam_detail_id')->withTrashed();
    }

    public function studyProgram()
    {
        return $this->belongsTo(StudyProgram::class, 'accepted_in')->withTrashed();
    }

    public function tries()
    {
        return $this->belongsToMany(StudyProgram::class, 'participant_study_program', 'participant_id', 'study_program_id', 'participant_id');
    }

    public function answerSummaries()
    {
        return $this->hasMany(AnswerSummary::class, 'exam_detail_id')->withTrashed();
    }
}
