<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamSchedule extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'start_time',
        'end_time',
        'cut_off',
        'summary',
        'note',
        'status'
    ];

    protected $dates = ['deleted_at'];

    public function details()
    {
        return $this->hasMany(ExamDetail::class)->withTrashed();
    }
}
