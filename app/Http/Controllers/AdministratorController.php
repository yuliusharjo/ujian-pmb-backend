<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Administrator;

class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Administrator::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'user_id' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_admin = $request->all();

        try {
            DB::transaction(function() use ($new_admin, &$admin) {
                $admin = Administrator::create($new_admin);
            });

            return Administrator::with(['user'])->find($admin->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Administrator::find($id);

        if (is_null($admin)) {
            return response()
                ->json(['errors' => ['Admin tidak ditemukan...']], 404);
        }

        return Administrator::with(['user'])->find($admin->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Administrator::find($id);

        if (is_null($admin)) {
            return response()
                ->json(['errors' => ['Admin tidak ditemukan...']], 404);
        }

        $new_admin = $request->all();

        try {
            DB::transaction(function() use ($new_admin, $admin) {
                $admin->update($new_admin);
            });

            return Administrator::with(['user'])->find($admin->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Administrator::find($id);

        if (is_null($admin)) {
            return response()
                ->json(['errors' => ['Admin tidak ditemukan...']], 404);
        }

        $admin->delete();

        return response()->json();
    }
}
