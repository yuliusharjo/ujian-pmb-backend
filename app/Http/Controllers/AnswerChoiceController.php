<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\AnswerChoice;

class AnswerChoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AnswerChoice::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_choice = $request->all();            

        try {
            DB::transaction(function() use ($new_choice, &$answer_choice) {
                $answer_choice = AnswerChoice::create($new_choice);
            });

            return AnswerChoice::find($answer_choice->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answer_choice = AnswerChoice::find($id);

        if (is_null($answer_choice)) {
            return response()
                ->json(['errors' => ['Pilihan jawaban tidak ditemukan...']], 404);
        }

        return AnswerChoice::with(['image'])->find($answer_choice->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer_choice = AnswerChoice::find($id);

        if (is_null($answer_choice)) {
            return response()
                ->json(['errors' => ['Pilihan jawaban tidak ditemukan...']], 404);
        }

        $new_choice = $request->all();

        try {
            DB::transaction(function() use ($new_choice, $answer_choice) {
                $answer_choice->update($new_choice);
            });

            return AnswerChoice::with(['image'])->find($answer_choice->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer_choice = AnswerChoice::find($id);
        
        if (is_null($answer_choice)) {
            return response()
                ->json(['errors' => ['Pilihan jawaban tidak ditemukan...']], 404);
        }

        $answer_choice->delete();

        return response()->json();
    }
}
