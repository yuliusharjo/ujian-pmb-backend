<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\QuestionCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('question_categories')->orderBy('id', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'category' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_category = $request->all();

        try {
            DB::transaction(function() use ($new_category, &$category) {
                $category = QuestionCategory::create($new_category);
            });

            return QuestionCategory::find($category->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = QuestionCategory::find($id);

        if (is_null($category)) {
            return response()
                ->json(['errors' => ['Kategori tidak ditemukan...']], 404);
        }

        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = QuestionCategory::find($id);

        if (is_null($category)) {
            return response()
                ->json(['errors' => ['Kategori tidak ditemukan...']], 404);
        }

        $new_category = $request->all();

        try {
            DB::transaction(function() use ($new_category, $category) {
                $category->update($new_category);
            });

            return QuestionCategory::find($category->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = QuestionCategory::find($id);

        if (is_null($category)) {
            return response()
                ->json(['errors' => ['Kategori tidak ditemukan...']], 404);
        }

        $category->delete();

        return response()->json();
    }
}
