<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\QuestionDiscourse;

class DiscourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('question_discourses')->where('deleted_at', '=', null)->orderBy('id', 'desc')->get();;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_discourse = $request->all();

        try {
            DB::transaction(function() use ($new_discourse, &$discourse) {
                $discourse = QuestionDiscourse::create($new_discourse);
            });

            return QuestionDiscourse::find($discourse->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $discourse = QuestionDiscourse::find($id);

        if (is_null($discourse)) {
            return response()
                ->json(['errors' => ['Wacana tidak ditemukan...']], 404);
        }

        return QuestionDiscourse::with(['image'])->find($discourse->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $discourse = QuestionDiscourse::find($id);

        if (is_null($discourse)) {
            return response()
                ->json(['errors' => ['Wacana tidak ditemukan...']], 404);
        }

        $new_discourse = $request->all();

        try {
            DB::transaction(function() use ($new_discourse, $discourse) {
                $discourse->update($new_discourse);
            });

            return QuestionDiscourse::find($discourse->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discourse = QuestionDiscourse::find($id);

        if (is_null($discourse)) {
            return response()
                ->json(['errors' => ['Wacana tidak ditemukan...']], 404);
        }

        $discourse->delete();

        return response()->json();
    }
}
