<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamAnswer;
use DB;
use Validator;

class ExamAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \DB::select('SELECT * FROM exam_answers');
        // return ExamAnswer::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'exam_detail_id' => 'required',
            'question_id' => 'required',
            'choice_index' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_answer = new ExamAnswer;
        $new_answer->exam_detail_id = $request->get('exam_detail_id');
        $new_answer->question_id = $request->get('question_id');
        $new_answer->choice_index = $request->get('choice_index');

        if ($request->has('exam_type'))
            $new_answer->exam_type = $request->get('exam_type');

        $correct_choice = DB::table('answer_choices')
                            ->select('index')
                            ->where([
                                ['question_id', '=', $new_answer->question_id],
                                ['correct_choice', '=', '1']
                            ])
                            ->get();

        $new_answer->is_correct = $request->get('choice_index') == $correct_choice[0]->index ? 1 : 0;

        try {
            $new_answer->save();

            // return ExamAnswer::find($new_answer->id);
            return response()
                ->json('Berhasil menyimpan jawaban', 200);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = ExamAnswer::find($id);

        if (is_null($answer)) {
            return response()
                ->json(['errors' => ['Jawaban Peserta tidak ditemukan...']], 404);
        }

        $exam_detail_id = $request->get('exam_detail_id');
        $question_id = $request->get('question_id');
        $choice_index = $request->get('choice_index');

        $correct_choice = DB::table('answer_choices')
                            ->select('index')
                            ->where([
                                ['question_id', '=', $question_id],
                                ['correct_choice', '=', '1']
                            ])
                            ->get();

        $is_correct = $choice_index == $correct_choice[0]->index ? 1 : 0;

        $new_answer = [
            'exam_detail_id' => $exam_detail_id,
            'question_id' => $question_id,
            'choice_index' => $choice_index,
            'is_correct' => $is_correct
        ];

        try {
            DB::transaction(function() use ($new_answer, $answer) {
                $answer->update($new_answer);
            });

            // return ExamAnswer::find($answer->id);
            return response()
                ->json('Berhasil mengubah jawaban', 200);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
