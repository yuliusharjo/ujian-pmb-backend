<?php

namespace App\Http\Controllers;

use App\ExamDetail;
use App\ExamAnswer;
use App\QuestionPacket;
use Illuminate\Http\Request;
use DB;
use stdClass;
use Validator;

class ExamDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = DB::table('exam_details')
            ->join('participants', 'exam_details.participant_id', '=', 'participants.id')
            ->join('users', 'participants.user_id', '=', 'users.id')
            ->join('question_packets', 'exam_details.packet_id', '=', 'question_packets.id')
            ->join('exam_schedules', 'exam_details.schedule_id', '=', 'exam_schedules.id')
            ->leftJoin('study_programs', 'exam_details.accepted_in', '=', 'study_programs.id')
            ->select(
                'exam_details.id',
                'participants.reg_id',
                'participants.old_reg_id',
                'participants.name',
                'question_packets.packet_code',
                'exam_schedules.summary as schedule',
                DB::raw('(CASE WHEN exam_details.total_score IS NULL THEN "-" ELSE total_score END) AS total_score'),
                DB::raw('(CASE WHEN exam_details.accepted_in IS NULL THEN "-" ELSE study_programs.name END) AS accepted_in')
            )
            ->orderBy('exam_details.id', 'desc')
            ->get();

        return $details;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'participants' => 'required',
            'packet_id' => 'required',
            'schedule_id' => 'required',
            'level' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_participants = [];

        foreach ($request->get('participants') as $participant) {
            array_push($new_participants, $participant);
        }

        try {
            $response = [];

            foreach ($new_participants as $new_participant) {
                $exam_detail = new ExamDetail;
                $exam_detail->level = $request->get('level');
                $exam_detail->schedule_id = $request->get('schedule_id');
                $exam_detail->packet_id = $request->get('packet_id');
                $exam_detail->participant_id = $new_participant;
                $exam_detail->save();

                array_push($response, $exam_detail);
            }

            return $response;
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $response = ExamDetail::with(['questionPacket', 'participant', 'schedule', 'examAnswers', 'studyProgram', 'tries'])->find($details->id);
        $userData = DB::table('users')->select('username')->where('id', $response->participant->user_id)->first();
        $response->user = $userData;

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExamDetail  $examDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamDetail $examDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $finishExam = $request->query('finish-exam');
        // $response = [];

        if ($finishExam) {
            // $answers = $details->examAnswers();
            // $questions = $details->questionPacket()->questions;
            $programChoices = $details->participant->studyPrograms;
            $total_score = 0;
            $accepted_in = null;
            // $scoresByCategory = [];

            foreach ($programChoices as $program) {
                $total_score = 0;
                $categories = $program->questionCategories;
                // $scoresByCategory = [];

                foreach ($categories as $category) {
                    if ($category->pivot->weight != 0) {
                        $questions = \App\QuestionPacket::find($details->questionPacket->id)
                            ->questions()
                            ->where('questions.category_id', '=', $category->id)
                            ->get();

                        $correctAnswers = DB::table('exam_answers')
                            ->join('questions', 'exam_answers.question_id', '=', 'questions.id')
                            ->select('exam_answers.*')
                            ->where([
                                ['exam_answers.exam_detail_id', '=', $details->id],
                                ['questions.category_id', '=', $category->id],
                                ['is_correct', '=', '1'],
                                ['exam_type', '=', 1]
                            ])
                            ->get();

                        $score = (count($correctAnswers) / count($questions)) * $category->pivot->weight;
                        $total_score += $score;
                        // $categoryScore = [$category, $score];

                        // array_push($scoresByCategory, $categoryScore);
                    }
                }

                if ($total_score >= $program->cut_off_points) {
                    $accepted_in = $program->id;
                    break;
                }
            }

            if ($accepted_in)
                $new_details = [
                    'accepted_in' => $accepted_in,
                    'total_score' => $total_score
                ];
            else
                $new_details = ['total_score' => $total_score];

            // array_push($response, $new_details);
            // array_push($response, $scoresByCategory);
        } else {
            $new_details = $request->all();
        }

        try {
            DB::transaction(function () use ($new_details, $details) {
                $details->update($new_details);
            });

            return ExamDetail::find($details->id);
            // return $response;
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function GenerateScores(Request $request, $id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $programChoices = $details->participant->studyPrograms;
        $total_score = 0;
        $accepted_in = null;
        $scores = [];

        foreach ($programChoices as $program) {
            $total_score = 0;
            $categories = $program->questionCategories;

            foreach ($categories as $category) {
                if ($category->pivot->weight != 0) {
                    $questions = \App\QuestionPacket::find($details->questionPacket->id)
                        ->questions()
                        ->where('questions.category_id', '=', $category->id)
                        ->get();

                    $correctAnswers = DB::table('exam_answers')
                        ->join('questions', 'exam_answers.question_id', '=', 'questions.id')
                        ->select('exam_answers.*')
                        ->where([
                            ['exam_answers.exam_detail_id', '=', $details->id],
                            ['questions.category_id', '=', $category->id],
                            ['is_correct', '=', '1'],
                            ['exam_type', '=', 1]
                        ])
                        ->get();

                    $score = (count($correctAnswers) / count($questions)) * $category->pivot->weight;
                    $total_score += $score;
                }
            }

            array_push($scores, ['score' => sprintf('%0.2f', $total_score), 'cut_off' => $program->cut_off_points, 'name' => $program->name, 'id' => $program->id]);
        }

        return response()->json($scores);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $details->delete();

        return response()->json();
    }

    public function showAnswers(Request $request, $id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $simulation = $request->query('simulation');
        $summary = $request->query('summary');

        if ($summary) {
            $categories = \App\QuestionCategory::all();
            $answerSummaries = [];

            foreach ($categories as $category) {
                // $questionsPerCategory = DB::table('exam_answers')
                //                         ->join('questions', 'exam_answers.question_id', '=', 'questions.id')
                //                         ->select('exam_answers.*')
                //                         ->where([
                //                             ['exam_answers.exam_detail_id', '=', $details->id],
                //                             ['questions.category_id', '=', $category->id]
                //                         ])
                //                         ->get();
                $questionsPerCategory = QuestionPacket::find($details->packet_id)
                    ->questions()
                    ->where('questions.category_id', '=', $category->id)
                    ->get();

                $correctAnswers = DB::table('exam_answers')
                    ->join('questions', 'exam_answers.question_id', '=', 'questions.id')
                    ->select('exam_answers.*')
                    ->where([
                        ['exam_answers.exam_detail_id', '=', $details->id],
                        ['questions.category_id', '=', $category->id],
                        ['is_correct', '=', '1']
                    ])
                    ->get();

                $answerSummary = new stdClass;

                $answerSummary->category = $category->category;
                $answerSummary->questions_total = count($questionsPerCategory);
                $answerSummary->correct_answers = count($correctAnswers);

                array_push($answerSummaries, $answerSummary);
            }
        } else {
            $answers = $simulation
                ? $details->examAnswers()->select('exam_answers.id', 'exam_answers.question_id', 'exam_answers.choice_index', 'exam_answers.exam_detail_id')->where('exam_type', '=', '2')->get()
                : $details->examAnswers()->select('exam_answers.id', 'exam_answers.question_id', 'exam_answers.choice_index', 'exam_answers.exam_detail_id')->where('exam_type', '=', '1')->get();
        }

        return $summary ? $answerSummaries : $answers;
    }

    public function storeSummaries(Request $request, $id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $categories = \App\QuestionCategory::all();
        $answerSummaries = [];

        foreach ($categories as $category) {
            $questionsPerCategory = QuestionPacket::find($details->packet_id)
                ->questions()
                ->where('questions.category_id', '=', $category->id)
                ->get();

            $correctAnswers = DB::table('exam_answers')
                ->join('questions', 'exam_answers.question_id', '=', 'questions.id')
                ->select('exam_answers.*')
                ->where([
                    ['exam_answers.exam_detail_id', '=', $details->id],
                    ['questions.category_id', '=', $category->id],
                    ['is_correct', '=', '1']
                ])
                ->get();

            $answerSummary = new \App\AnswerSummary;

            $answerSummary->exam_detail_id = $details->id;
            $answerSummary->category_id = $category->id;
            $answerSummary->questions_total = count($questionsPerCategory);
            $answerSummary->correct_answers = count($correctAnswers);

            array_push($answerSummaries, $answerSummary);
        }

        try {
            DB::transaction(function () use ($answerSummaries, $details) {
                $details->answerSummaries()->saveMany($answerSummaries);
            });

            return ExamDetail::with(['answerSummaries'])->find($details->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function showSummaries($id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $summaries = $details->answerSummaries;
        $response = [];

        foreach ($summaries as $summary) {
            if ($summary->questions_total != 0) {
                $category = \App\QuestionCategory::find($summary->category_id);
                $summary->category_name = $category->category;
                array_push($response, $summary);
            }
        }

        return $response;
    }

    public function deleteSummaries($id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        DB::table('answer_summaries')->where('exam_detail_id', '=', $details->id)->delete();

        return response()->json();
    }

    public function deleteAnswers(Request $request, $id)
    {
        $details = ExamDetail::find($id);

        if (is_null($details)) {
            return response()
                ->json(['errors' => ['Detail Ujian tidak ditemukan...']], 404);
        }

        $simulation = $request->query('simulation');

        $simulation ?
            DB::table('exam_answers')->where([
                ['exam_detail_id', '=', $details->id],
                ['exam_type', '=', 2]
            ])->delete()
            :
            DB::table('exam_answers')->where('exam_detail_id', '=', $details->id)->delete();

        return response()->json();
    }

    public function storeEmptyAnswers(Request $request, $id)
    {
        $new_answers = [];

        foreach ($request->get('answers') as $answer) {
            array_push($new_answers, new ExamAnswer($answer));
        }

        try {
            DB::transaction(function () use (&$exam_detail, $id, $new_answers) {
                $exam_detail = ExamDetail::find($id);
                $exam_detail->examAnswers()
                    ->saveMany($new_answers);
            });

            return ExamDetail::with(['examAnswers'])->find($id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
