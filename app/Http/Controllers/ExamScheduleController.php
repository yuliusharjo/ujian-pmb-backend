<?php

namespace App\Http\Controllers;

use App\ExamSchedule;
use DB;
use Validator;
use Illuminate\Http\Request;

class ExamScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('exam_schedules')->select(
            'start_time',
            'end_time',
            'summary',
            'cut_off',
            'created_at',
            'updated_at',
            'id',
            DB::raw('(CASE WHEN status = 1 THEN "Aktif" ELSE "Tidak Aktif" END) AS status'),
            DB::raw('(CASE WHEN note IS NULL THEN "-" ELSE note END) AS note')
        )->where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'start_time' => 'required',
            'end_time' => 'required',
            'summary' => 'required',
            'cut_off' => 'required',
            'status' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_schedule = $request->all();

        if (strtotime($request->get('end_time')) - strtotime($request->get('start_time')) <= 0) {
            return response()->json('Waktu selesai tidak boleh lebih awal atau sama dengan waktu mulai!', 400);
        }

        try {
            DB::transaction(function() use ($new_schedule, &$schedule) {
                $schedule = ExamSchedule::create($new_schedule);
            });

            return ExamSchedule::find($schedule->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = ExamSchedule::find($id);

        if (is_null($schedule)) {
            return response()
                ->json(['errors' => ['Jadwal Ujian tidak ditemukan...']], 404);
        }

        return $schedule;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExamSchedule  $examSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamSchedule $examSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int  $id)
    {
        $schedule = ExamSchedule::find($id);

        if (is_null($schedule)) {
            return response()
                ->json(['errors' => ['Jadwal Ujian tidak ditemukan...']], 404);
        }

        $new_schedule = $request->all();

        if (strtotime($request->get('end_time')) - strtotime($request->get('start_time')) <= 0) {
            return response()->json('Waktu selesai tidak boleh lebih awal atau sama dengan waktu mulai!', 400);
        }

        try {
            DB::transaction(function() use ($new_schedule, $schedule) {
                $schedule->update($new_schedule);
            });

            return ExamSchedule::find($schedule->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = ExamSchedule::find($id);

        if (is_null($schedule)) {
            return response()
                ->json(['errors' => ['Jadwal Ujian tidak ditemukan...']], 404);
        }

        $schedule->delete();

        return response()->json();
    }
}
