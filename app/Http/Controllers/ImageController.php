<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Image;
use Validator;
use DB;

class ImageController extends Controller
{
    public function index()
    {
        return Image::all();
    }

    public function show($id)
    {
        $image = Image::find($id);

        if (is_null($image)) {
            return response()
                ->json(['errors' => ['Gambar tidak ditemukan']], 404);
        }

        return $image;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'file_name' => 'required|file',
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        $image = new Image;
        $lastImageId = Image::all()->last() ? Image::all()->last()->id : 0;

        $files = $request->file('file_name');
        // $name = $files->getClientOriginalName();
        $extention = $files->getClientOriginalExtension();
        $name = 'image-' . $lastImageId . '.' . $extention;
        $destinationPath = public_path('/images');
        $files->move($destinationPath, $name);
        $image->file_name = $name;

        try {
            $image->save();

            return Image::find($image->id);
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $image = Image::find($id);

        if (is_null($image)) {
            return response()
                ->json(['errors' => ['Gambar tidak ditemukan...']], 404);
        }

        File::delete('images/' . $image->file_name);

        $image->delete();

        return response()->json();
    }
}
