<?php

namespace App\Http\Controllers;

use App\Jobs\GenerateDocz;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use DB;
use Validator;
use App\QuestionPacket;
use App\Question;
use App\QuestionCategory;
use Illuminate\Support\Facades\Storage;

class PacketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $level = $request->query('level');

        $packets = DB::table('question_packets')
                        ->leftJoin('study_programs', 'study_programs.id', '=', 'question_packets.study_program_id')
                        ->select('question_packets.id',
                                'question_packets.packet_code',
                                'question_packets.study_program_id',
                                DB::raw('(CASE WHEN question_packets.level = 1 THEN "S1" ELSE "S2" END) AS level'),
                                DB::raw('(CASE WHEN question_packets.study_program_id IS NULL THEN "All S1" ELSE study_programs.name END) AS study_program'))
                        ->where('question_packets.deleted_at', '=', null)
                        ->when($level, function ($query, $level) {
                            return $query->where('question_packets.level', $level);
                        })
                        ->orderBy('question_packets.id', 'desc')
                        ->get();

        return $packets;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'packet_code' => 'required',
            'level' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_packet = $request->all();

        try {
            DB::transaction(function() use ($new_packet, &$packet) {
                $packet = QuestionPacket::create($new_packet);
            });

            return QuestionPacket::find($packet->id);
        } catch (QueryException $e) {
            return response()
                ->json(['errors' => ['Kode Paket Soal sudah digunakan. Silakan buat kode baru...']], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $packet = QuestionPacket::find($id);

        if (is_null($packet)) {
            return response()
                ->json(['errors' => ['Paket Soal tidak ditemukan...']], 404);
        }


        $response = $packet;
        $questions = [];

        foreach ($packet->questions as $q){
            $data = $q;
            $category = DB::table('question_categories')->select('category')->where('id', $q->category_id)->first();
            $level = $data->level == 1 ? 'S1' : 'S2';

            $data->category = $category->category;
            $data->level = $level;

            array_push($questions, $data);
        }

        $response->questions = $questions;

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $packet = QuestionPacket::find($id);

        if (is_null($packet)) {
            return response()
                ->json(['errors' => ['Paket Soal tidak ditemukan...']], 404);
        }

        $new_packet = $request->all();

        try {
            DB::transaction(function() use ($new_packet, $packet) {
                $packet->update($new_packet);
            });

            return QuestionPacket::find($packet->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $packet = QuestionPacket::find($id);

        if (is_null($packet)) {
            return response()
                ->json(['errors' => ['Paket Soal tidak ditemukan...']], 404);
        }

        $packet->delete();

        return response()->json();
    }

    public function addQuestions(Request $request, $id)
    {
        $packet = QuestionPacket::find($id);

        if (is_null($packet)) {
            return response()
                ->json(['errors' => ['Paket Soal tidak ditemukan...']], 404);
        }

        $questions = $request->get('questions');

        try {
            $packet->questions()->attach($questions);

            return $packet->questions;
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function removeQuestions(Request $request, $id)
    {
        $packet = QuestionPacket::find($id);

        if (is_null($packet)) {
            return response()
                ->json(['errors' => ['Paket Soal tidak ditemukan...']], 404);
        }

        // $question = $request->get('questions');

        try {
            $packet->questions()->detach($packet->questions);

            return $packet->questions;
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function GetFileDocz($packet_id)
    {
        $packet = QuestionPacket::find($packet_id);
        //$packet->packet_code.docx        
        if (Storage::exists('docz/'.$packet->packet_code.'.docx.zip'))  return Storage::download('docz/'.$packet->packet_code.'.docx.zip');
        return response('no file', 400);
        
    }

    public function GenerateDocz($packet_id)
    {
        $packet = QuestionPacket::find($packet_id);
        $docpath = 'docz/'.$packet->packet_code.'.docx';
        $zippath = 'docz/'.$packet->packet_code.'.docx.zip';
        $keypath = 'docz/'.$packet->packet_code.'-key.docx';

        if(Storage::exists($docpath)) Storage::delete($docpath);
        if(Storage::exists($keypath)) Storage::delete($keypath);
        if(Storage::exists($zippath)) Storage::delete($zippath);
        GenerateDocz::dispatch(['name' => $packet->packet_code, 'packet'=> $packet_id]);
        return response('Processing Packet ....');
    }

    public function showQuestions(Request $request, $id)
    {
        $packet = QuestionPacket::find($id);

        if (is_null($packet)) {
            return response()
                ->json(['errors' => ['Paket tidak ditemukan...']], 404);
        }

        $onExam = $request->query('on-exam');
        $categories = QuestionCategory::all();
        $response = [];

        foreach ($categories as $c) {
            $questions = QuestionPacket::find($packet->id)
                            ->questions()
                            ->where('questions.category_id', '=', $c->id)
                            ->inRandomOrder()
                            ->get();

            foreach ($questions as $q) {
                $category = QuestionCategory::find($q->category_id);
                $discourse = \App\QuestionDiscourse::with(['image'])->find($q->discourse_id);
                $image = \App\Image::find($q->question_image_id);
                $answerChoices = DB::table('answer_choices')
                                    ->select('id',
                                        'index',
                                        'choice_text',
                                        'choice_image_id',
                                        'choice_image_name',
                                        'question_id')
                                    ->where('question_id', '=', $q->id)
                                    ->inRandomOrder()
                                    ->get();

                $q->discourse = $discourse;
                $q->image = $image;
                $q->category = $category->category;
                $q->answer_choices = $answerChoices;

                array_push($response, $q);
            }
        }

        $questions = $packet->questions;

        return $onExam ? $response : $questions;
    }
}
