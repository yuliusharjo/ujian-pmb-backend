<?php

namespace App\Http\Controllers;

use App\ExamDetail;
use Illuminate\Http\Request;
use DB;
use Validator;
use App\Participant;
use App\User;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $level = $request->query('level');
        $forExamDetail = $request->query('exam-detail');
        $examDetails = ExamDetail::all();
        $participantHasExams = [];

        if ($forExamDetail == true) {
            foreach ($examDetails as $exam) {
                array_push($participantHasExams, $exam->participant_id);
            }
        }
        if($level == '2'){
            $participants = DB::table('users')
            ->join('participants', 'users.id', '=', 'participants.user_id')
            ->leftJoin('participant_study_program', 'participants.id', '=','participant_study_program.participant_id')
            ->leftJoin('study_programs', 'study_programs.id', '=','participant_study_program.study_program_id')
            ->select(
                'participants.id',
                'participants.name',
                'participants.year_of_entry',
                DB::raw('(CASE WHEN participant_study_program.study_program_id IS NULL THEN "-" ELSE participant_study_program.study_program_id END) AS study_program_id'),
                DB::raw('(CASE WHEN study_programs.name IS NULL THEN "-" ELSE study_programs.name END) AS program_name'),
                DB::raw('(CASE WHEN participants.status_khusus = 1 THEN "Ya" ELSE "Tidak" END) AS status_khusus'),
                DB::raw('(CASE WHEN participants.type = 1 THEN "Reguler" ELSE "OTS" END) AS type'),
                DB::raw('(CASE WHEN participants.level = 1 THEN "S1" ELSE "S2" END) AS level'),
                DB::raw("CONCAT(participants.exam_date, ' Sesi ', participants.exam_session) AS exam_schedule"),
                DB::raw('(CASE WHEN participants.status = 1 THEN "aktif" ELSE "tidak aktif" END) AS status'),
                'users.username',
                DB::raw('(CASE WHEN participants.old_reg_id IS NULL THEN "0" ELSE participants.old_reg_id END) AS old_reg_id'))
            ->when($level, function ($query, $level) {
                return $query->where('participants.level', $level);
            })
            ->when($participantHasExams, function ($query, $participantHasExams) {
                return $query->whereNotIn('participants.id', $participantHasExams)->where('participants.status', 1);
            })
            ->orderBy('participants.id', 'desc')
            ->get();
        }else{
            $participants = DB::table('users')
                                ->join('participants', 'users.id', '=', 'participants.user_id')
                                ->select('participants.id',
                                    'participants.name',
                                    'participants.year_of_entry',
                                    DB::raw('(CASE WHEN participants.status_khusus = 1 THEN "Ya" ELSE "Tidak" END) AS status_khusus'),
                                    DB::raw('(CASE WHEN participants.type = 1 THEN "Reguler" ELSE "OTS" END) AS type'),
                                    DB::raw('(CASE WHEN participants.level = 1 THEN "S1" ELSE "S2" END) AS level'),
                                    DB::raw("CONCAT(participants.exam_date, ' Sesi ', participants.exam_session) AS exam_schedule"),
                                    DB::raw('(CASE WHEN participants.status = 1 THEN "aktif" ELSE "tidak aktif" END) AS status'),
                                    'users.username',
                                    DB::raw('(CASE WHEN participants.old_reg_id IS NULL THEN "0" ELSE participants.old_reg_id END) AS old_reg_id'))
                                ->when($level, function ($query, $level) {
                                    return $query->where('level', $level);
                                })
                                ->when($participantHasExams, function ($query, $participantHasExams) {
                                    return $query->whereNotIn('participants.id', $participantHasExams)->where('participants.status', 1);
                                })
                                ->orderBy('participants.id', 'desc')
                                ->get();
        }

        return $participants;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'level' => 'required',
            'user_id' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_participant = $request->all();

        try {
            DB::transaction(function() use ($new_participant, &$participant) {
                $participant = Participant::create($new_participant);
            });

            return Participant::with(['user'])->find($participant->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $participant = Participant::find($id);

        if (is_null($participant)) {
            return response()
                ->json(['errors' => ['Peserta tidak ditemukan...']], 404);
        }

        return Participant::with(['user', 'studyPrograms', 'examDetail'])->find($participant->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $participant = Participant::find($id);

        if (is_null($participant)) {
            return response()
                ->json(['errors' => ['Peserta tidak ditemukan...']], 404);
        }

        $new_participant = $request->all();

        try {
            DB::transaction(function() use ($new_participant, $participant) {
                $participant->update($new_participant);
            });

            $programs = $request->get('study_program_id');
            $participant->studyPrograms()->detach();
            if($participant->level == 2){                
                $participant->studyPrograms()->sync($programs);
            }
            else{
                if (is_array($programs)) {
                    for ($i=0; $i < count($programs); $i++) { 
                        # code...
                        $participant->studyPrograms()->attach($programs[$i]);
                    }
                }
                
            }

            return Participant::with(['user'])->find($participant->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function updateLogout(Request $request, $id)
    {
        $participant = Participant::find($id);

        if (is_null($participant)) {
            return response()
                ->json(['errors' => ['Peserta tidak ditemukan...']], 404);
        }

        $new_participant = $request->all();

        try {
            DB::transaction(function() use ($new_participant, $participant) {
                $participant->update($new_participant);
            });

            return Participant::with(['user'])->find($participant->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participant = Participant::find($id);

        if (is_null($participant)) {
            return response()
                ->json(['errors' => ['Peserta tidak ditemukan...']], 404);
        }

        $participant->delete();

        return response()->json();
    }

    public function addStudyPrograms(Request $request, $id)
    {
        $participant = Participant::find($id);

        if (is_null($participant)) {
            return response()
                ->json(['errors' => ['Peserta Soal tidak ditemukan...']], 404);
        }

        if (count($request->get('programs')) > 3) {
            return response()
                ->json(['errors' => ['Jumlah pilihan tidak boleh lebih dari 3...']], 400);
        }

        $programs = $request->get('programs');

        try {
            $participant->studyPrograms()->attach($programs);

            return Participant::with(['studyPrograms'])->find($participant->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function storeMany(Request $request)
    {
        $storeOne = $request->query('store-one');

        if ($storeOne) {
            $validation = Validator::make($request->all(), [
                'level' => 'required',
                'user_id' => 'required',
            ]);

            if ($validation->fails()) {
                return response()
                    ->json($validation->errors(), 422);
            }

            $new_participant = $request->all();
        } else {
            $new_users = [];

            foreach ($request->get('users') as $user) {
                array_push($new_users, $user);
            }

            $type = 1;

            if ($request->has('type'))
                $type = 2;
        }

        try {
            if ($storeOne) {
                DB::transaction(function() use ($new_participant, &$participant) {
                    $participant = Participant::create($new_participant);
                });
                if($participant->level == 2){
                    $programs = $request->get('study_program_id');
                    $participant->studyPrograms()->attach($programs);
                }
                return Participant::with(['user'])->find($participant->id);
            } else {
                foreach ($new_users as $new_user) {
                    $user = User::create($new_user);

                    $participant = new Participant;
                    $participant->level = $request->get('level');
                    $participant->status_khusus = $request->get('status_khusus');
                    $participant->type = $type;
                    $participant->exam_date = $request->get('exam_date');
                    $participant->exam_session = $request->get('exam_session');
                    $participant->user_id = $user->id;
                    $participant->reg_id = $user->username;
                    $participant->save();
                    if($participant->level == 2){
                        $programs = $request->get('study_program_id');
                        $participant->studyPrograms()->attach($programs);
                    }
                }

                return response()->json();
            }
        } catch (\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
