<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Question;
use App\AnswerChoice;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $level = $request->query('level');

        $questions = DB::table('questions')
                        ->join('question_categories', 'question_categories.id', '=', 'questions.category_id')
                        ->select('questions.id',
                                'questions.question',
                                'question_categories.category',
                                DB::raw('(CASE WHEN questions.level = 1 THEN "S1" ELSE "S2" END) AS level'))
                        ->where('questions.deleted_at', '=', null)
                        ->when($level, function ($query, $level) {
                            return $query->where('level', $level);
                        })
                        ->orderBy('questions.id', 'desc')
                        ->get();

        return $questions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'question' => 'required',
            'category_id' => 'required',
            'level' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_question = $request->all();

        try {
            DB::transaction(function() use ($new_question, &$question) {
                $question = Question::create($new_question);
            });

            return Question::find($question->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $question = Question::find($id);

        if (is_null($question)) {
            return response()
                ->json(['errors' => ['Pertanyaan tidak ditemukan...']], 404);
        }

        $onExam = $request->query('on-exam');
        $answerChoices = DB::table('answer_choices')->select('id', 'index', 'choice_text', 'choice_image_id', 'choice_image_name', 'question_id')->where('question_id', $question->id)->inRandomOrder()->get();

        $response = Question::with(['image', 'category', 'discourse', 'discourse.image'])->find($question->id);
        $onExam ? $response->answer_choices = $answerChoices : $response->answer_choices = $question->answerChoices;

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'question' => 'required',
            'category_id' => 'required',
            'level' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $question = Question::find($id);

        if (is_null($question)) {
            return response()
                ->json(['errors' => ['Pertanyaan tidak ditemukan...']], 404);
        }

        $new_question = $request->all();

        try {
            DB::transaction(function() use ($new_question, $question) {
                $question->update($new_question);
            });

            return Question::find($question->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);

        if (is_null($question)) {
            return response()
                ->json(['errors' => ['Pertanyaan tidak ditemukan...']], 404);
        }

        $question->delete();

        return response()->json();
    }

    public function showAnswerChoices($id)
    {
        $question = Question::find($id);

        if (is_null($question)) {
            return response()
                ->json(['errors' => ['Pertanyaan tidak ditemukan...']], 404);
        }

        return $question->answerChoices;
    }

    public function addAnswerChoices(Request $request, $id)
    {
        $new_answers = [];

        foreach ($request->get('answer_choices') as $answer_choice) {
            array_push($new_answers, new AnswerChoice($answer_choice));
        }

        try {
            DB::transaction(function() use (&$question, $id, $new_answers) {
                $question = Question::find($id);
                $question->answerChoices()
                    ->saveMany($new_answers);
            });

            return Question::with(['answerChoices'])->find($id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
