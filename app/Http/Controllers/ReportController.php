<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participant;
use App\ExamDetail;
use Illuminate\Database\QueryException;
use DB;

class ReportController extends Controller
{
    public function getRegisteredUsers(Request $request) {
        $startDate = $request->query('start-date');
        $endDate = $request->query('end-date');

        try {
            $report = Participant::select(
                        DB::raw('MONTH(created_at) month'),
                        DB::raw('count(*) as total')
                    )
                    ->groupBy('month')
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->orderBy('month')
                    ->get();

            return $report;
        } catch (QueryException $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function getPassedUsers(Request $request) {
        $startDate = $request->query('start-date');
        $endDate = $request->query('end-date');

        try {
            $report = ExamDetail::select(
                        DB::raw('MONTH(created_at) month'),
                        DB::raw('count(*) as total')
                    )
                    ->groupBy('month')
                    ->whereNotNull('accepted_in')
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->orderBy('month')
                    ->get();

            return $report;
        } catch (QueryException $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
