<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\StudyProgram;

class StudyProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return StudyProgram::all();
        $level = $request->query('level');

        $programs = DB::table('study_programs')
                        ->select('*',
                            DB::raw('(CASE WHEN level = 1 THEN "S1" WHEN level = 2 THEN "S2" ELSE "0" END) AS level_in_text'),
                            DB::raw('(CASE WHEN class_type = 1 THEN "Reguler" ELSE "Internasional" END) AS class_in_text'))
                        ->when($level, function ($query, $level) {
                            if ($level == 1)
                                return $query->where('level', '=', '1');
                            else
                                return $query->where('level', '=', '2');
                        })
                        ->orderBy('program_code', 'asc')
                        ->get();

        return $programs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'program_code' => 'required'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_program = $request->all();

        try {
            DB::transaction(function() use ($new_program, &$program) {
                $program = StudyProgram::create($new_program);
            });

            return StudyProgram::find($program->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $program = StudyProgram::find($id);

        if (is_null($program)) {
            return response()
                ->json(['errors' => ['Program Study tidak ditemukan...']], 404);
        }

        return StudyProgram::with(['questionCategories'])->find($program->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program = StudyProgram::find($id);

        if (is_null($program)) {
            return response()
                ->json(['errors' => ['Program Studi tidak ditemukan...']], 404);
        }

        $new_program = $request->all();

        try {
            DB::transaction(function() use ($new_program, $program) {
                $program->update($new_program);
            });

            return StudyProgram::find($program->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = StudyProgram::find($id);

        if (is_null($program)) {
            return response()
                ->json(['errors' => ['Program studi tidak ditemukan...']], 404);
        }

        $program->delete();

        return response()->json();
    }

    public function resolveQuestionCategory(Request $request, $id) {
        $program = StudyProgram::find($id);

        if (is_null($program)) {
            return response()
                ->json(['errors' => ['Program Studi tidak ditemukan...']], 404);
        }

        $action = $request->get('action');
        $category = $request->get('category');
        $weight = $request->get('weight');

        try {
            if ($action) {
                if (strtoupper($action) == strtoupper('new')) {
                    $program->questionCategories()->attach($category);
                } else if (strtoupper($action) == strtoupper('update')) {
                    if (count($category) != count($weight)) {
                        return response()
                            ->json(['errors' => ['Kategori dan bobot tidak sesuai...']], 400);
                    } else {
                        for ($i = 0; $i < count($category); $i++) {
                            $program->questionCategories()->updateExistingPivot($category[$i], ['weight' => $weight[$i]]);
                        }
                    }
                } else {
                    $program->questionCategories()->detach($category);
                }
            } else {
                return response()
                    ->json(['errors' => ['Aksi belum ditentukan...']], 400);
            }

            return $action == 'update' ? $program->questionCategories : $program->questionCategories->find($category);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
