<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $new_user = $request->all();

        try {
            DB::transaction(function() use ($new_user, &$user) {
                $user = User::create($new_user);
            });

            return User::with(['role'])->find($user->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        if (is_null($user)) {
            return response()
                ->json(['errors' => ['Pengguna tidak ditemukan...']], 404);
        }

        return User::with([
            'participant',
            'participant.studyPrograms',
            'participant.examDetail',
            'participant.examDetail.studyProgram',
            'participant.examDetail.schedule'
        ])->find($user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()
                ->json(['errors' => ['Pengguna tidak ditemukan...']], 404);
        }

        $new_user = $request->all();

        try {
            DB::transaction(function() use ($new_user, $user) {
                $user->update($new_user);
            });

            return User::with(['role'])->find($user->id);
        } catch (Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()
                ->json(['errors' => ['Pengguna tidak ditemukan...']], 404);
        }

        $user->delete();

        return response()->json();
    }
}
