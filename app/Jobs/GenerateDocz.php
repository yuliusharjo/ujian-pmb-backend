<?php

namespace App\Jobs;

use App\Docz\WordGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateDocz implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    private $args = ['name' => '', 'packet' => 1];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args)
    {
        $this->args = $args;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        $document = new WordGenerator();
        $document->GeneratePacket($this->args['packet']);
        $docpath = 'storage/app/docz/'.$this->args['name'].'.docx';
        $keypath = 'storage/app/docz/'.$this->args['name'].'-key.docx';
        $document->Save($docpath, $keypath);
                        
        dump('Complete Jobs , '. $this->args['name']);
        $time_elapsed_secs = microtime(true) - $start;
        dump($time_elapsed_secs);
    }
}
