<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participant extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'reg_id',
        'old_reg_id',
        'name',
        'level',
        'user_id',
        'status',
        'type',
        'year_of_entry',
        'exam_date',
        'exam_session',
        'status_khusus',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function studyPrograms()
    {
        return $this->belongsToMany(StudyProgram::class)->withTrashed();
    }

    public function examDetail()
    {
        return $this->hasOne(ExamDetail::class)->withTrashed();
    }
}
