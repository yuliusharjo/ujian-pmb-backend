<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'question',
        'category_id',
        'discourse_id',
        'question_image_id',
        'level',
        'hint'
    ];

    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo(QuestionCategory::class, 'category_id')->withTrashed();
    }

    public function discourse()
    {
        return $this->belongsTo(QuestionDiscourse::class, 'discourse_id')->withTrashed();
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'question_image_id')->withTrashed();
    }

    public function answerChoices()
    {
        return $this->hasMany(AnswerChoice::class, 'question_id')->withTrashed();
    }

    public function examAnswers()
    {
        return $this->hasMany(ExamAnswer::class, 'question_id')->withTrashed();
    }

    public function packets()
    {
        return $this->belongsToMany(QuestionPacket::class)->withTrashed();
    }
}
