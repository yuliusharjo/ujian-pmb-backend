<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'category'
    ];

    protected $dates = ['deleted_at'];

    public function questions()
    {
        return $this->hasMany(Question::class, 'category_id')->withTrashed();
    }

    public function studyPrograms()
    {
        return $this->belongsToMany(StudyProgram::class)->withPivot(['weight'])->withTimeStamps()->withTrashed();
    }

    public function answerSummaries()
    {
        return $this->hasMany(AnswerSummary::class, 'category_id')->withTrashed();
    }
}
