<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class QuestionDiscourse extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'title',
        'body',
        'image_id',
    ];

    protected $dates = ['deleted_at'];

    public function questions()
    {
        return $this->hasMany(Question::class, 'discourse_id')->withTrashed();
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id')->withTrashed();
    }
}
