<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionPacket extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'packet_code',
        'level',
        'study_program_id'
    ];

    protected $dates = ['deleted_at'];

    public function questions()
    {
        return $this->belongsToMany(Question::class)->withTrashed();
    }

    public function examDetails()
    {
        return $this->hasMany(ExamDetail::class)->withTrashed();
    }

    public function studyProgram(){
        return $this->belongsTo('App/StudyProgram','study_program_id','id');
    }
}
