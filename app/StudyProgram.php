<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudyProgram extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'program_code',
        'level',
        'class_type',
        'cut_off_points'
    ];

    protected $dates = ['deleted_at'];

    public function participants()
    {
        return $this->belongsToMany(Participant::class)->withTrashed();
    }

    public function questionCategories()
    {
        return $this->belongsToMany(QuestionCategory::class)->withPivot(['weight'])->withTimestamps()->withTrashed();
    }

    public function examDetails()
    {
        return $this->hasMany(ExamDetail::class, 'accepted_in')->withTrashed();
    }
}
