<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Participant;
use App\User;
use Faker\Generator as Faker;

$factory->define(Participant::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'level' => array_random([1, 2]),
        'user_id' => factory(User::class)->create()->id,
        'exam_date' => date("Y-m-d", strtotime(array_random(["+2 Weeks","+3 Weeks"]))),
        // 'year_of_entry' => date("Y", strtotime("+1 Year")),
        // 'exam_date' => array_random([$date->addWeeks(2), $date->addWeeks(3)]),
        'exam_session' => array_random([1, 2])
    ];
});
