<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\QuestionCategory;
use Faker\Generator as Faker;

$factory->define(QuestionCategory::class, function (Faker $faker) {
    return [
        'category' => $faker->unique()->word
    ];
});
