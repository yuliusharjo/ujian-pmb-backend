<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\QuestionDiscourse;
use Faker\Generator as Faker;

$factory->define(QuestionDiscourse::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
        'body' => $faker->paragraph
    ];
});
