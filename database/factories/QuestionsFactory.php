<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Question;
use App\QuestionCategory;
use App\QuestionDiscourse;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'question' => $faker->paragraph,
        'level' => rand(1, 2),
        'category_id' => function () {
            return QuestionCategory::inRandomOrder()->first()->id;
        },
        'discourse_id' => array_random([QuestionDiscourse::inRandomOrder()->first()->id, null]),
        'hint' => array_random([$faker->paragraph, null]),
    ];
});
