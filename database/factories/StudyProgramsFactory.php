<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\StudyProgram;
use Faker\Generator as Faker;

$factory->define(StudyProgram::class, function (Faker $faker) {
    return [
        'name' => array_random([$faker->words($nb = 2, $asText = true), 'Magister ' . $faker->words($nb = 2, $asText = true)]),
        'program_code' => $faker->numerify('###'),
        'cut_off_points' => $faker->randomFloat(2, 50, 100),
    ];
});
