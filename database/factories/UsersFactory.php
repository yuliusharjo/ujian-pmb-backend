<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->numerify('########'),
        'password' => bcrypt('participant'),
        'role_id' => 2
    ];
});
