<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reg_id')->unique()->nullable();
            $table->string('old_reg_id')->unique()->nullable();
            $table->string('name')->default('anonim');
            $table->tinyInteger('level');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('type')->default(1); // 1 = Reguler, 2 = OTS
            $table->string('year_of_entry')->default(date("Y"));
            $table->date('exam_date');
            $table->tinyInteger('exam_session');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
