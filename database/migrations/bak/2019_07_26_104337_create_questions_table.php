<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('question');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('discourse_id')->nullable();
            $table->unsignedBigInteger('question_image_id')->nullable();
            $table->tinyInteger('level');
            $table->longText('hint')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')
                ->references('id')->on('question_categories')
                ->onDelete('cascade');

            $table->foreign('discourse_id')
                ->references('id')->on('question_discourses')
                ->onDelete('cascade');

            $table->foreign('question_image_id')
                ->references('id')->on('images')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
