<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerChoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_choices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('index');
            $table->longText('choice_text')->nullable();
            $table->unsignedBigInteger('choice_image_id')->nullable();
            $table->string('choice_image_name')->nullable();
            $table->boolean('correct_choice')->default(false);
            $table->unsignedBigInteger('question_id')->nullable();
            $table->timestamps();
            $table->softdeletes();

            $table->foreign('question_id')
                ->references('id')->on('questions')
                ->onDelete('cascade');

            $table->foreign('choice_image_id')
                ->references('id')->on('images')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_choices');
    }
}
