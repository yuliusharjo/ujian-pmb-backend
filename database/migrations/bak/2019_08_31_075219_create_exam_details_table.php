<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('participant_id');
            $table->unsignedBigInteger('packet_id');
            $table->unsignedBigInteger('schedule_id');
            $table->tinyInteger('level');
            $table->decimal('total_score', 5, 2)->nullable();
            $table->unsignedBigInteger('accepted_in')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('participant_id')
                ->references('id')->on('participants')
                ->onDelete('cascade');

            $table->foreign('packet_id')
                ->references('id')->on('question_packets')
                ->onDelete('cascade');

            $table->foreign('schedule_id')
                ->references('id')->on('exam_schedules')
                ->onDelete('cascade');

            $table->foreign('accepted_in')
                ->references('id')->on('study_programs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_details');
    }
}
