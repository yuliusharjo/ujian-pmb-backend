<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionCategoryStudyProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_category_study_program', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_category_id');
            $table->unsignedBigInteger('study_program_id');
            $table->decimal('weight', 4, 2)->default(0);
            $table->timestamps();

            $table->foreign('question_category_id')
                ->references('id')->on('question_categories')
                ->onDelete('cascade');

            $table->foreign('study_program_id')
                ->references('id')->on('study_programs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_category_study_program');
    }
}
