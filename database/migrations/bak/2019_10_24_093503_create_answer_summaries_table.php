<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_summaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('exam_detail_id');
            $table->unsignedBigInteger('category_id');
            $table->integer('questions_total');
            $table->integer('correct_answers');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('exam_detail_id')
                ->references('id')->on('exam_details')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('question_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_summaries');
    }
}
