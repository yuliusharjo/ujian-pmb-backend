<?php

use Illuminate\Database\Seeder;
use App\Administrator;

class AdministratorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Administrator::create([
            'name' => 'Krisna Sandi Saputra',
            'user_id' => 1
        ]);
    }
}
