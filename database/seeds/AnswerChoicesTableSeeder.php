<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\AnswerChoice;
use App\Question;

class AnswerChoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $questions = Question::all();

        foreach ($questions as $question) {
            AnswerChoice::create([
                'index' => 1,
                'choice_text' => $faker->sentence,
                'correct_choice' => true,
                'question_id' => $question->id
            ]);

            for ($i = 0; $i < 4; $i++) {
                AnswerChoice::create([
                    'index' => $i + 2,
                    'choice_text' => $faker->sentence,
                    'correct_choice' => false,
                    'question_id' => $question->id
                ]);
            }
        }
    }
}
