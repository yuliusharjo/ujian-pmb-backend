<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            QuestionCategoriesTableSeeder::class,
            QuestionDiscoursesTableSeeder::class,
            QuestionsTableSeeder::class,
            AnswerChoicesTableSeeder::class,
            QuestionPacketsTableSeeder::class,
            RolesTableSeeder::class,
            StudyProgramsTableSeeder::class,
            ParticipantsTableSeeder::class,
            UsersTableSeeder::class,
            AdministratorsTableSeeder::class,
            ExamSchedulesTableSeeder::class,
            ExamDetailsTableSeeder::class
        ]);
    }
}
