<?php

use Illuminate\Database\Seeder;
use App\ExamDetail;
use App\Participant;
use App\QuestionPacket;
use App\ExamSchedule;

class ExamDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $participants = Participant::all();

        foreach ($participants as $participant) {
            ExamDetail::create([
                'packet_id' => QuestionPacket::where('level', '=', $participant->level)->inRandomOrder()->first()->id,
                'participant_id' => $participant->id,
                'level' => $participant->level,
                'schedule_id' => ExamSchedule::inRandomOrder()->first()->id
            ]);
        }
    }
}
