<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\ExamSchedule;

class ExamSchedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startTime = Carbon::today()->addHours(10);
        $endTime = Carbon::today()->addHours(12);

        $schedule = new ExamSchedule;
        $schedule->start_time = $startTime->addWeeks(2);
        $schedule->end_time = $endTime->addWeeks(2);
        $schedule->summary = $schedule->start_time . (' - ') . $schedule->end_time->format('H:i:s') . (' WIB');
        $schedule->cut_off = 120;
        $schedule->save();

        $schedule2 = new ExamSchedule;
        $schedule2->start_time = $startTime->addWeeks(1);
        $schedule2->end_time = $endTime->addWeeks(1);
        $schedule2->summary = $schedule2->start_time . (' - ') . $schedule2->end_time->format('H:i:s') . (' WIB');
        $schedule2->cut_off = 120;
        $schedule2->save();
    }
}
