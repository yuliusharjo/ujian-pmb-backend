<?php

use App\Participant;
use App\StudyProgram;
use App\User;
use Illuminate\Database\Seeder;
// use Faker\Factory as Faker;

class ParticipantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Participant::class, 20)->create();

        $participants = Participant::all();
        $users = User::all();

        foreach ($participants as $p) {
            $level = $p->level;

            $p->studyPrograms()
                ->attach(StudyProgram::when($level, function($query, $level) {
                    if ($level == 1)
                        return $query->where('name', 'not like', 'magister%');
                    else
                        return $query->where('name', 'like', 'magister%');
                })
                ->inRandomOrder()->take(array_random([1, 2, 3]))->get());
        }

        foreach($users as $user) {
            $reg_id = $user->username;
            $participant = Participant::find($user->participant['id']);

            $participant->reg_id = $reg_id;
            $participant->save();
        }
        // $faker = Faker::create();

        // $participant = new Participant;
        // $participant->name = 'Naruto Uzumaki';
        // $participant->level = 1;
        // $participant->user_id = 2;
        // $participant->exam_date = date("Y-m-d", strtotime("+2 Weeks"));
        // $participant->exam_session = array_random([1, 2]);
        // $participant->save();
        // $participant->studyPrograms()
        //     ->attach(StudyProgram::where('name', 'not like', 'magister%')
        //     ->inRandomOrder()->take(3)->get());

        // $participant2 = new Participant;
        // $participant2->name = 'Sasuke Uchiha';
        // $participant2->level = 2;
        // $participant2->user_id = 3;
        // $participant2->exam_date = date("Y-m-d", strtotime("+2 Weeks"));
        // $participant2->exam_session = array_random([1, 2]);
        // $participant2->save();
        // $participant2->studyPrograms()
        //     ->attach(StudyProgram::where('name', 'like', 'magister%')
        //     ->inRandomOrder()->take(3)->get());
    }
}
