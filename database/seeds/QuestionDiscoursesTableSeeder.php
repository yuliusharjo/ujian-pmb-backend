<?php

use Illuminate\Database\Seeder;

class QuestionDiscoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\QuestionDiscourse::class, 20)->create();
    }
}
