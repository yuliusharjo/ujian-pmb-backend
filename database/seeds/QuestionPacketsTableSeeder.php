<?php

use Illuminate\Database\Seeder;
use App\QuestionPacket;
use App\Question;

class QuestionPacketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packet = new QuestionPacket;
        $packet->packet_code = 'S1-1';
        $packet->level = 1;
        $packet->save();
        $packet->questions()->attach(Question::where('level', '=', 1)->inRandomOrder()->take(100)->get());

        $packet2 = new QuestionPacket;
        $packet2->packet_code = 'S1-2';
        $packet2->level = 1;
        $packet2->save();
        $packet2->questions()->attach(Question::where('level', '=', 1)->inRandomOrder()->take(100)->get());

        $packet3 = new QuestionPacket;
        $packet3->packet_code = 'S2-1';
        $packet3->level = 2;
        $packet3->save();
        $packet3->questions()->attach(Question::where('level', '=', 2)->inRandomOrder()->take(100)->get());
    }
}
