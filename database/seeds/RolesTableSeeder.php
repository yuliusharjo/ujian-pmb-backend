<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    protected $roles = [
        ['id' => 1, 'role' => 'Administrator'],
        ['id' => 2, 'role' => 'Participant'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
            Role::create($role);
        }
    }
}
