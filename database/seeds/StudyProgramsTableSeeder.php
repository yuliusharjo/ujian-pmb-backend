<?php

use App\StudyProgram;
// use App\QuestionCategory;
use Illuminate\Database\Seeder;

class StudyProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(StudyProgram::class, 10)->create();

        // $programs = StudyProgram::all();
        // $categories = QuestionCategory::all();

        // foreach ($programs as $p) {
        //     $p->questionCategories()->attach($categories, ['weight' => 100 / count($categories)]);
        // }
    }
}
