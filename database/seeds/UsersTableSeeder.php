<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        User::create([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'role_id' => 1
        ]);

        // User::create([
        //     'username' => $faker->numerify('########'),
        //     'password' => bcrypt('participant1'),
        //     'role_id' => 2
        // ]);

        // User::create([
        //     'username' => $faker->numerify('########'),
        //     'password' => bcrypt('participant2'),
        //     'role_id' => 2
        // ]);
    }
}
