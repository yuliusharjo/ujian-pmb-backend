<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, enctype');
// header('Access-Control-Allow-Methods: GET, PATCH, POST, DELETE');

Route::group(['middleware' => 'api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

    Route::group(['middleware' => ['auth']], function () {

        Route::get('categories', 'CategoryController@index');
        Route::get('categories/{id}', 'CategoryController@show');
        Route::post('categories', 'CategoryController@store');
        Route::put('categories/{id}', 'CategoryController@update');
        Route::delete('categories/{id}', 'CategoryController@destroy');

        Route::get('questions', 'QuestionController@index');
        Route::get('questions/{id}', 'QuestionController@show');
        Route::post('questions', 'QuestionController@store');
        Route::put('questions/{id}', 'QuestionController@update');
        Route::delete('questions/{id}', 'QuestionController@destroy');
        Route::get('questions/{id}/answer-choices', 'QuestionController@showAnswerChoices');
        Route::put('questions/{id}/answer-choices', 'QuestionController@addAnswerChoices');

        Route::get('discourses', 'DiscourseController@index');
        Route::get('discourses/{id}', 'DiscourseController@show');
        Route::post('discourses', 'DiscourseController@store');
        Route::put('discourses/{id}', 'DiscourseController@update');
        Route::delete('discourses/{id}', 'DiscourseController@destroy');

        Route::get('answer-choices', 'AnswerChoiceController@index');
        Route::get('answer-choices/{id}', 'AnswerChoiceController@show');
        Route::post('answer-choices', 'AnswerChoiceController@store');
        Route::put('answer-choices/{id}', 'AnswerChoiceController@update');
        Route::delete('answer-choices/{id}', 'AnswerChoiceController@destroy');

        Route::get('packets', 'PacketController@index');
        Route::get('packets/{id}', 'PacketController@show');
        Route::post('packets', 'PacketController@store');
        Route::put('packets/{id}', 'PacketController@update');
        Route::delete('packets/{id}', 'PacketController@destroy');
        Route::get('packets/{id}/questions', 'PacketController@showQuestions');
        Route::post('packets/{id}/questions', 'PacketController@addQuestions');
        Route::delete('packets/{id}/questions', 'PacketController@removeQuestions');
        Route::get('packets/download/{packet_id}', 'PacketController@GetFileDocz');
        Route::get('packets/generate/{packet_id}', 'PacketController@GenerateDocz');

        Route::get('users', 'UserController@index');
        Route::get('users/{id}', 'UserController@show');
        Route::post('users', 'UserController@store');
        Route::put('users/{id}', 'UserController@update');
        Route::delete('users/{id}', 'UserController@destroy');

        Route::get('participants', 'ParticipantController@index');
        Route::get('participants/{id}', 'ParticipantController@show');
        Route::post('participants', 'ParticipantController@storeMany');\
        Route::put('participants/{id}', 'ParticipantController@update');
        Route::delete('participants/{id}', 'ParticipantController@destroy');
        Route::put('participants/{id}/programs', 'ParticipantController@addStudyPrograms');
        Route::put('participantsLogout/{id}', 'ParticipantController@updateLogout');

        Route::get('images', 'ImageController@index');
        Route::get('images/{id}', 'ImageController@show');
        Route::post('images', 'ImageController@store');
        Route::delete('images/{id}', 'ImageController@destroy');

        Route::get('programs', 'StudyProgramController@index');
        Route::get('programs/{id}', 'StudyProgramController@show');
        Route::post('programs', 'StudyProgramController@store');
        Route::put('programs/{id}', 'StudyProgramController@update');
        Route::delete('programs/{id}', 'StudyProgramController@destroy');
        Route::post('programs/{id}/categories', 'StudyProgramController@resolveQuestionCategory');

        Route::get('admins', 'AdministratorController@index');
        Route::get('admins/{id}', 'AdministratorController@show');
        Route::post('admins', 'AdministratorController@store');
        Route::put('admins/{id}', 'AdministratorController@update');
        Route::delete('admins/{id}', 'AdministratorController@destroy');

        Route::get('schedules', 'ExamScheduleController@index');
        Route::post('schedules', 'ExamScheduleController@store');
        Route::get('schedules/{id}', 'ExamScheduleController@show');
        Route::put('schedules/{id}', 'ExamScheduleController@update');
        Route::delete('schedules/{id}', 'ExamScheduleController@destroy');

        Route::get('exam-details', 'ExamDetailController@index');
        Route::post('exam-details', 'ExamDetailController@store');
        Route::get('exam-details/{id}', 'ExamDetailController@show');
        Route::put('exam-details/{id}', 'ExamDetailController@update');
        Route::delete('exam-details/{id}', 'ExamDetailController@destroy');
        Route::get('exam-details/{id}/exam-answers', 'ExamDetailController@showAnswers');
        Route::put('exam-details/{id}/exam-answers', 'ExamDetailController@storeEmptyAnswers');
        Route::delete('exam-details/{id}/exam-answers', 'ExamDetailController@deleteAnswers');
        Route::post('exam-details/{id}/answer-summaries', 'ExamDetailController@storeSummaries');
        Route::get('exam-details/{id}/answer-summaries', 'ExamDetailController@showSummaries');
        Route::delete('exam-details/{id}/answer-summaries', 'ExamDetailController@deleteSummaries');
        Route::get('exam-details/scores/{id}', 'ExamDetailController@GenerateScores');

        Route::get('exam-answers', 'ExamAnswerController@index');
        Route::post('exam-answers', 'ExamAnswerController@store');
        Route::get('exam-answers/{id}', 'ExamAnswerController@show');
        Route::put('exam-answers/{id}', 'ExamAnswerController@update');
        Route::delete('exam-answers/{id}', 'ExamAnswerController@destroy');

        Route::get('reports/registered-users', 'ReportController@getRegisteredUsers');
        Route::get('reports/passed-users', 'ReportController@getPassedUsers');
    });
});
