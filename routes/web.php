<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Docz\WordGenerator;
// use App\Jobs\GenerateDocz;
// use App\QuestionPacket;
use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Storage;

Route::get('/', function () {
    view('welcome');
});
// Route::get('/foo', function () {
//     $args = ['name' => 'test-3.0', 'packet' => 5];
//     $start = microtime(true);
//     $document = new WordGenerator();
//     $document->GeneratePacket($args['packet']);
//     $docpath = 'storage/app/docz/' . $args['name'] . '.docx';
//     $keypath = 'storage/app/docz/' . $args['name'] . '-key.docx';
//     $document->Save($docpath, $keypath);

//     dump('Complete Jobs , ' . $args['name']);
//     $time_elapsed_secs = microtime(true) - $start;
//     dump($time_elapsed_secs);
// });

// Route::get('/moo', function () {
//     GenerateDocz::dispatch(['name' => 'SoalTest', 'packet' => 5]);
//     return response('Processing Packet ....');
// });

// Route::get('/doo', function () {
//     return Storage::download('docz/S2-1.docx.zip');
// });
