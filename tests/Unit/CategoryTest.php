<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetCategoriesWithMiddleware()
    {
        $response = $this->json('GET', '/api/categories');
        $response->assertStatus(401);
    }

    public function testGetCategories()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'api')->json('GET', '/api/categories');
        $response->assertStatus(200);
    }

    public function testCreateInvalidCategory()
    {
        $data = [
            'category' => "",
        ];

        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'api')->json('POST', '/api/categories', $data);
        $response->assertStatus(422);
    }

    public function testCreateCategory()
    {
        $data = [
            'category' => "Art"
        ];

        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'api')->json('POST', '/api/categories', $data);
        $response->assertStatus(200);
    }

    public function testUpdateNonExistingCategory()
    {
        $data = [
            'category' => "Sport",
        ];

        $user = factory(\App\User::class)->create();
        $categories = \App\QuestionCategory::all();
        $response = $this->actingAs($user, 'api')->json('PUT', '/api/categories/'.($categories[count($categories) - 1]->id + 1), $data);
        $response->assertStatus(404);
    }

    public function testUpdateCategory()
    {
        $data = [
            'category' => "Sport"
        ];

        $user = factory(\App\User::class)->create();
        $categories = \App\QuestionCategory::all();

        if (count($categories) > 0) {
            $response = $this->actingAs($user, 'api')->json('PUT', '/api/categories/'.($categories[0]->id), $data);
            $response->assertStatus(200);
        } else {
            $response = $this->actingAs($user, 'api')->json('PUT', '/api/categories/'.($categories[0]->id), $data);
            $response->assertStatus(404);
        }
    }
}
