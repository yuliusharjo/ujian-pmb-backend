<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetQuestionsWithMiddleware()
    {
        $response = $this->json('GET', '/api/questions');
        $response->assertStatus(401);
    }

    public function testGetQuestions()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'api')->json('GET', '/api/questions');
        $response->assertStatus(200);
    }

    public function testCreateInvalidQuestion()
    {
        $data = [
            'question' => "What's up?",
        ];

        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'api')->json('POST', '/api/questions', $data);
        $response->assertStatus(422);
    }

    public function testCreateQuestion()
    {
        $data = [
            'question' => "What's up?",
            'category_id' => 1,
            'level' => 1
        ];

        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'api')->json('POST', '/api/questions', $data);
        $response->assertStatus(200);
    }

    public function testUpdateNonExistingQuestion()
    {
        $data = [
            'question' => "What's down?",
            'category_id' => 1,
            'level' => 1
        ];

        $user = factory(\App\User::class)->create();
        $question = \App\Question::all();
        $response = $this->actingAs($user, 'api')->json('PUT', '/api/questions/'.($question[count($question) - 1]->id + 1), $data);
        $response->assertStatus(404);
    }

    public function testUpdateQuestion()
    {
        $data = [
            'question' => "What's down?",
            'category_id' => 1,
            'level' => 1
        ];

        $user = factory(\App\User::class)->create();
        $question = \App\Question::all();

        if (count($question) > 0) {
            $response = $this->actingAs($user, 'api')->json('PUT', '/api/questions/'.($question[0]->id), $data);
            $response->assertStatus(200);
        } else {
            $response = $this->actingAs($user, 'api')->json('PUT', '/api/questions/'.($question[0]->id), $data);
            $response->assertStatus(404);
        }
    }

    public function testDeleteNonExistingQuestion()
    {
        $user = factory(\App\User::class)->create();

        $question = \App\Question::all();
        $response = $this->actingAs($user, 'api')->json('DELETE', '/api/questions/'.($question[count($question) - 1]->id + 1));
        $response->assertStatus(404);
    }

    public function testDeleteQuestion()
    {
        $user = factory(\App\User::class)->create();

        $question = \App\Question::all();

        if (count($question) > 0) {
            $response = $this->actingAs($user, 'api')->json('DELETE', '/api/questions/'.($question[0]->id));
            $response->assertStatus(200);
        } else {
            $response = $this->actingAs($user, 'api')->json('DELETE', '/api/questions/'.($question[0]->id));
            $response->assertStatus(404);
        }
    }

    public function testShowAnswerChoices()
    {
        $user = factory(\App\User::class)->create();

        $question = \App\Question::all();

        if (count($question) > 0) {
            $response = $this->actingAs($user, 'api')->json('GET', '/api/questions/'.($question[0]->id).'/answer-choices');
            $response->assertStatus(200);
        } else {
            $response = $this->actingAs($user, 'api')->json('GET', '/api/questions/'.($question[0]->id).'/answer-choices');
            $response->assertStatus(404);
        }
    }

    // public function testAddAnswerChoices()
    // {
    //     $user = factory(\App\User::class)->create();

    //     $questionData = [
    //         'question' => "Test question?",
    //         'category_id' => 1,
    //         'level' => 1
    //     ];
    //     $answerData = [
    //         'choice_text',
    //         'correct_choice',
    //         'question_id'
    //     ];
    //     $answerId = [];

    //     $question = $this->actingAs($user, 'api')->json('POST', '/api/questions', $questionData);

    //     for ($i = 0; $i < 5; $i++) {
    //         $answerData['choice_text'] = 'This is choice '.$i;
    //         $i == 0 ? $answerData['correct_choice'] = 1 : 0;
    //         $answerData['question_id'] = $question->id;

    //         $answer = $this>actingAs($user, 'api')->json('POST', '/api/answer-choices', $answerData);
    //         array_push($answerId, $answer->id);
    //     }

    //     $response->assertStatus(200);
    // }
}
